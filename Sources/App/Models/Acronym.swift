import Vapor
import FluentPostgreSQL

final class Acronym: Codable {
  var id: Int? // id set by database when saved
  var short: String
  var long: String

  init(short: String, long: String) {
    self.short = short
    self.long = long
  }
}

extension Acronym: PostgreSQLModel {}
// Replaces
//extension Acronym: Model {
//  typealias ID = Int // Tell Fluent type of ID
//  typealias Database = SQLiteDatabase // Tell Fluent what DB to use for this model
//  public static var idKey: IDKey = \Acronym.id // Tell Fluent the key path of the model's ID property
//}

// Allow to save model to DB
extension Acronym: Migration {}

extension Acronym: Content {}
